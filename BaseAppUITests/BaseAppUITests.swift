//
//  BaseAppUITests.swift
//  BaseAppUITests
//
//  Created by Henderson on 2021/06/11.
//  Copyright © 2021 Henderson. All rights reserved.
//

import XCTest

class BaseAppUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        let app = XCUIApplication()
        app.launch()        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
//        let app = XCUIApplication()
//        app.launch()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        element.children(matching: .textField).matching(identifier: "아이디").element(boundBy: 0).tap()
        
        app.typeText("123")
        element.tap()
        app/*@START_MENU_TOKEN@*/.staticTexts["로그인"].press(forDuration: 0.6);/*[[".buttons[\"로그인\"].staticTexts[\"로그인\"]",".tap()",".press(forDuration: 0.6);",".staticTexts[\"로그인\"]"],[[[-1,3,1],[-1,0,1]],[[-1,2],[-1,1]]],[0,0]]@END_MENU_TOKEN@*/
        app/*@START_MENU_TOKEN@*/.staticTexts["Small Bottom Sheet"]/*[[".buttons[\"Small Bottom Sheet\"].staticTexts[\"Small Bottom Sheet\"]",".staticTexts[\"Small Bottom Sheet\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        element.children(matching: .other).element(boundBy: 1).children(matching: .other).element(boundBy: 0).swipeDown()
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
