//
//  BaseViewModel.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/07.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation

class BaseViewModel {
    
}

protocol BaseDelegate {
    func showLoadingBar()
    func hideLoadingBar()
    func showDialog(message: String?)
}
