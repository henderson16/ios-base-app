//
//  LoginViewController_.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/01.
//  Copyright © 2021 Henderson. All rights reserved.
//

import UIKit
import MaterialComponents
import RxSwift
import RxCocoa

class LoginViewController: KeyboardViewController, LoginDelegate {
    let viewModel = LoginViewModel()
    
    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var idCopyTextField: UITextField!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordCopyTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    //TODO: frame의 위치가 정확히 나오는 곳은 viewDidAppear임
    //관련해서 찾아보기....
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initBinding()
    }
    
    private func initView() {
        viewModel.delegate = self
        idTextField.delegate = self
        idCopyTextField.delegate = self
        passwordTextField.delegate = self
        passwordCopyTextField.delegate = self
    }
    
    private func initBinding() {
        (idTextField.rx.text<->viewModel.userId).disposed(by: disposeBag)
        viewModel.userId.asObservable()
            .subscribe(onNext: { data in
                self.viewModel.isChecked.accept(!data.isEmpty)
                self.viewModel.observedUserId.accept(data)
            }).disposed(by: disposeBag)
        
        viewModel.observedUserId.bind(to: idCopyTextField.rx.text).disposed(by: disposeBag)
        viewModel.isChecked.map{ return !$0 }.bind(to: checkImage.rx.isHidden).disposed(by: disposeBag)
        
        loginButton.rx.tap.bind {
            self.viewModel.clickLogin()
        }.disposed(by: disposeBag)
    }
    
    private func setView() {
        
    }
    
    //Login Delegate
    override func showLoadingBar() {
        super.showLoadingBar()
    }
    
    override func hideLoadingBar() {
        super.hideLoadingBar()
    }
    
    func showDialog(message: String?) {
        let alert = AlertDialog(nibName: "AlertDialog", bundle: nil)
        if let message = message {
            alert.initAlert(message: message)
        } else {
            alert.initAlert(message: "popup_network_error".localized)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToMain() {
        moveWithRemoving(to: MainViewController())
    }
    
    //Set Max Length to TextField
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
            case idTextField:
                let maxLength = 4
                let currentString: NSString = (textField.text ?? "") as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            case passwordTextField:
                let maxLength = 6
                let currentString: NSString = (textField.text ?? "") as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            default:
                return true
        }
    }
}
