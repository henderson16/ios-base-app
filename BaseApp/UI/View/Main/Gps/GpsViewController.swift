//
//  GpsViewController.swift
//  BaseApp
//
//  Created by Henderson on 2021/09/15.
//  Copyright © 2021 Henderson. All rights reserved.
//

import UIKit
import CoreLocation

class GpsViewController: BaseViewController, CLLocationManagerDelegate {
    let viewModel = GpsViewModel()
    
    var locationManager: CLLocationManager!
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initBinding()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
    }
    
    private func initBinding() {
        //        button.rx.tap.bind{
        //            self.requestGpsPermission()
        //        }.disposed(by: disposeBag)
    }

    private func startLocationService() {
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.showsBackgroundLocationIndicator = true
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            startLocationService()
        case .restricted, .notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        case .denied:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager,  didUpdateLocations locations: [CLLocation]) {
        let lastLocation = locations.last!
        locationLabel.text = "\(lastLocation.coordinate.latitude), \(lastLocation.coordinate.longitude)"
        print("location : \(lastLocation)")
    }
}
