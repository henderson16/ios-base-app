//
//  BleViewController.swift
//  BaseApp
//
//  Created by Henderson on 2022/01/18.
//  Copyright © 2022 Henderson. All rights reserved.
//

import UIKit
import CoreBluetooth


class BleScanViewController: BaseViewController, BleDelegate {

    let viewModel = BleViewModel.shared
    
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var deviceList =  [CBPeripheral]()
    var rssiList =  [NSNumber]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initBinding()
    }
    
    private func initView() {
        viewModel.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        let bleDeviceCell = UINib(nibName: "BleDeviceCell", bundle: nil)
        tableView.register(bleDeviceCell, forCellReuseIdentifier: "BleDeviceCell")
    }
    
    private func initBinding() {
        viewModel.isScanning.map{ $0 ? "stop" : "start" }.bind(to: scanButton.rx.title()).disposed(by: disposeBag)
        
        scanButton.rx.tap.bind {
            self.clickScanButton()
        }.disposed(by: disposeBag)
    }
    
    private func clickScanButton() {
        viewModel.scanBle()
    }
    
    func scannedDevice(device: CBPeripheral, rssi: NSNumber) {
        deviceList.append(device)
        rssiList.append(rssi)
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: deviceList.count-1, section: 0)], with: .automatic)
        tableView.endUpdates()
    }
    
    func didConnect() {
        go(to: BleControlViewController())
    }
    
    func didDisconnect() { }
    
    func readData(command: CommandType, data: String) { }
}

extension BleScanViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.connectToDevice(device: deviceList[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BleDeviceCell", for: indexPath) as! BleDeviceCell
        
        let index = indexPath.row
        cell.initView(name: deviceList[index].name, address: deviceList[index].identifier.uuidString, rssi: rssiList[index])

        return cell
    }
}
