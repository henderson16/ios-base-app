//
//  PageViewController.swift
//  BaseApp
//
//  Created by Henderson on 2021/10/14.
//  Copyright © 2021 Henderson. All rights reserved.
//

import UIKit

class PageViewController: UIViewController {
    
    @IBOutlet weak var pageView: UIView!
    var myPageViewController: MyPageViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    private func initView() {
        myPageViewController = MyPageViewController()
        
        addChild(myPageViewController)
        
        pageView.addSubview(myPageViewController.view)
        myPageViewController.view.frame = pageView.bounds
        myPageViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction func clickFirstButton(_ sender: Any) {
        myPageViewController.setViewControllers([myPageViewController.pages[0]], direction: .forward, animated: true, completion: nil)
    }
    
    @IBAction func clickSecondButton(_ sender: Any) {
        myPageViewController.setViewControllers([myPageViewController.pages[1]], direction: .forward, animated: true, completion: nil)
    }
    
    @IBAction func clickThirdButton(_ sender: Any) {
        myPageViewController.setViewControllers([myPageViewController.pages[2]], direction: .forward, animated: true, completion: nil)
    }
}

class MyPageViewController: UIPageViewController {
    let pages: [UIViewController] = [
        FirstPageViewController(),
        SecondPageViewController(),
        ThirdPageViewController()
    ]
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = nil
        
        setViewControllers([pages[0]], direction: .forward, animated: false, completion: nil)
    }
}

extension MyPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else { return pages.last }
        
        guard pages.count > previousIndex else { return nil }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return pages.first }
        
        guard pages.count > nextIndex else { return nil }
        
        return pages[nextIndex]
    }
}

extension MyPageViewController: UIPageViewControllerDelegate {
    // if you do NOT want the built-in PageControl (the "dots"), comment-out these funcs
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstVC = pageViewController.viewControllers?.first else {
            return 0
        }
        guard let firstVCIndex = pages.firstIndex(of: firstVC) else {
            return 0
        }

        return firstVCIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        print("page \(pageViewController)")
        print("page \(pendingViewControllers)")
    }
}
