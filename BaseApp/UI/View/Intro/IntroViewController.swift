//
//  IntroVC.swift
//  BaseApp
//
//  Created by Henderson on 2019/12/16.
//  Copyright © 2019 Henderson. All rights reserved.
//

import UIKit

class IntroViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        goToNext()
    }
    
    private func goToNext() {
        moveWithRemoving(to: LoginViewController())
    }
//    @IBAction func goToNext(_ sender: UIButton) {
//        let intro = LoginVC(nibName: "LoginView", bundle: nil)
//        self.navigationController?.pushViewController(intro, animated: true)
//        //        self.navigationController?.viewControllers.remove(at: 0)
//    }
//
//    @IBAction func goToMvvm(_ sender: UIButton) {
//        let next = SearchC(nibName: "SearchView", bundle: nil)
//        self.navigationController?.pushViewController(next, animated: true)
//    }
//    @IBAction func goToMvvmTest(_ sender: UIButton) {
//        let next = MvvmC(nibName: "MvvmView", bundle: nil)
//        self.navigationController?.pushViewController(next, animated: true)
//    }
}
