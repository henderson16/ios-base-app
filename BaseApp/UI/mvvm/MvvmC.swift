//
//  MvvmTest.swift
//  BaseApp
//
//  Created by Henderson on 2020/07/24.
//  Copyright © 2020 Henderson. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class MvvmC: UIViewController {
    
    private var viewModel = MvvmVM()
    let disposeBag = DisposeBag()
    
    let searchController = UISearchController(searchResultsController: nil)
    var searchBar: UISearchBar { return searchController.searchBar }
    
    @IBOutlet weak var mainTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSearchController();
        
        initView()
    }
    
    private func initView() {
        let nibName = UINib(nibName: "RepoCell", bundle: nil)
        mainTable.register(nibName, forCellReuseIdentifier: "repoCell")
        
        viewModel.data
            .drive(mainTable.rx.items(cellIdentifier: "repoCell")) { _, repository, cell in
                cell.textLabel?.text = repository.repoName
                cell.detailTextLabel?.text = repository.repoName
        }
        .addDisposableTo(disposeBag)
        
        searchBar.rx.text.orEmpty
            .bind(to: viewModel.searchText)
            .disposed(by: disposeBag)
    }
    
    private func configureSearchController() {
        searchController.obscuresBackgroundDuringPresentation = false
        searchBar.showsCancelButton = true
        searchBar.text = "NavdeepSinghh"
        searchBar.placeholder = "Enter GitHub ID, e.g., \"NavdeepSinghh\""
        mainTable.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
    }
}
