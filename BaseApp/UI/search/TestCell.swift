//
//  TestCell.swift
//  BaseApp
//
//  Created by Henderson on 2020/06/17.
//  Copyright © 2020 Henderson. All rights reserved.
//

import UIKit

class TestCell: UITableViewCell {

    @IBOutlet weak var testCellTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
