//
//  BleDeviceCell.swift
//  BaseApp
//
//  Created by Henderson on 2022/01/18.
//  Copyright © 2022 Henderson. All rights reserved.
//

import UIKit

class BleDeviceCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initView(name: String?, address: String, rssi: NSNumber) {
        title.text = "name : \(name)  \naddress : \(address) \nrssi : \(rssi)"
    }
}
