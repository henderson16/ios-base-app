//
//  FilterResponse.swift
//  BaseApp
//
//  Created by gtsoft on 2023/10/19.
//  Copyright © 2023 Henderson. All rights reserved.
//

import Foundation

/// 특정 키로 여러 타입의 값이 들어오는 경우 init에서 분기
struct FilterResponse: Codable {
    var resultMsg = ""
    var resultCode = 0
    var function = ""
    var certPath: String? = nil
    var deviceId: String? = nil
    var authType: String? = nil
    var authPassword: String? = nil
    var publicKey: String? = nil
    var encData: String? = nil
    var changeEncData: String? = nil
    var randomKey: String? = nil
    var keypadSafeMode: Int? = nil
    var digitalSignature: String? = nil
    var digitalSignatures: [String]? = nil
    
    enum CodingKeys: String, CodingKey {
        case resultMsg
        case resultCode
        case function       = "func"
        case certPath
        case deviceId
        case authType
        case authPassword
        case publicKey
        case encData
        case changeEncData
        case randomKey
        case keypadSafeMode
        case digitalSignature
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        resultMsg = try container.decode(String.self, forKey: .resultMsg)
        resultCode = try container.decode(Int.self, forKey: .resultCode)
        function = try container.decode(String.self, forKey: .function)
        certPath = (try? container.decode(String.self, forKey: .certPath)) ?? nil
        deviceId = (try? container.decode(String.self, forKey: .deviceId)) ?? nil
        authType = (try? container.decode(String.self, forKey: .authType)) ?? nil
        authPassword = (try? container.decode(String.self, forKey: .authPassword)) ?? nil
        publicKey = (try? container.decode(String.self, forKey: .publicKey)) ?? nil
        encData = (try? container.decode(String.self, forKey: .encData)) ?? nil
        changeEncData = (try? container.decode(String.self, forKey: .changeEncData)) ?? nil
        randomKey = (try? container.decode(String.self, forKey: .randomKey)) ?? nil
        keypadSafeMode = (try? container.decode(Int.self, forKey: .keypadSafeMode)) ?? nil
        
        if let string = try? container.decode([String].self, forKey: .digitalSignature) {
            digitalSignature = "\(string)"
        } else {
            digitalSignature = (try? container.decode(String.self, forKey: .digitalSignature)) ?? nil
        }
    }
}
