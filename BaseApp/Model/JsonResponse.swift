//
//  CheckResponse.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/09.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation

class JsonResponse: BaseResponse {
    var name: String?
    var age: String?
    var data: String?
    
    private enum CodingKeys: String, CodingKey {
        case name
        case age
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try? container.decode(String.self, forKey: .name)
        age = try? container.decode(String.self, forKey: .age)
        data = try? container.decode(String.self, forKey: .data)
        try super.init(from: decoder)
    }
}
