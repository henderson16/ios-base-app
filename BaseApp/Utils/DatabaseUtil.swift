//
//  DatabaseUtil.swift
//  BaseApp
//
//  Created by Henderson on 2021/06/23.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class DatabaseUtil {
    func saveUserInfo() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "UserInfo", in: context)!
        
        let userInfo = NSManagedObject(entity: entity, insertInto: context)
        
        userInfo.setValue("4", forKey: "id")
        userInfo.setValue("44", forKey: "password")
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save. \(error)")
        }
    }
    
    func fetchUserInfo() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserInfo")
        
        do {
            let result = try context.fetch(fetchRequest)
            for data in result as! [NSManagedObject]{
                print("id: \(data.value(forKey: "id") as! String)")
                print("pw: \(data.value(forKey: "password") as! String)")
            }
        } catch let error as NSError{
            print("Could not fetch. \(error)")
        }
    }
    
    func deleteUserInfo(id: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        
        let deleteRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserInfo")
        deleteRequest.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let result = try context.fetch(deleteRequest)
            
            let objectToDelete = result[0] as! NSManagedObject
            context.delete(objectToDelete)
            do {
                try context.save()
            } catch let error as NSError{
                print("Could not fetch. \(error)")
            }
        } catch let error as NSError{
            print("Could not fetch. \(error)")
        }
    }
}
