//
//  BaseKey.swift
//  BaseApp
//
//  Created by gtsoft on 5/13/24.
//  Copyright © 2024 Henderson. All rights reserved.
//

import Foundation

enum LanguageKey {
    case kr
    case en
}

class BaseKey {
    static let success      = "0000"
    static let error        = "9999"             // 네트워크 오류
    
#if RELEASE
    static let serverUrl = "www.release.com"    //운영
#else
    static let serverUrl = "www.dev.com"        //개발
#endif
    
}
