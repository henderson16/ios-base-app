//
//  BaseUtil.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/09.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation
import os
import AVFoundation
import MediaPlayer


class BaseUtil {
    static let shared = BaseUtil()
    
    func getVersionCode() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    func playSound() {
        MPVolumeView.setVolume(0.1)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0) { [self] in
            do {
                try AVAudioSession.sharedInstance().setCategory(.playAndRecord, options: [.duckOthers, .defaultToSpeaker])
                try AVAudioSession.sharedInstance().setActive(true)
                UIApplication.shared.beginReceivingRemoteControlEvents()

//                let url = URL(fileURLWithPath: Bundle.main.path(forResource: "gas.wav", ofType: nil)!)
                let audioURL = URL(string: "/System/Library/Audio/UISounds/Tock.caf")
                let url = URL(string: "/System/Library/Audio/UISounds/new-mail.caf")
                let player = try AVAudioPlayer(contentsOf: audioURL!)
                player.play()
            } catch {
                printLog("playSound error: \(error)")
            }
        }
    }
        
    func playVibration() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    func printLog(_ message: String) {
        #if DEBUG
        os_log("!! %s %s", type: .default, String(describing: type(of: self)), message)
        #endif
    }
}

extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume
        }
    }
}
