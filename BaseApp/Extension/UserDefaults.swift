//
//  UserDefaults.swift
//  BaseApp
//
//  Created by henderson on 2022/06/09.
//  Copyright © 2022 Henderson. All rights reserved.
//

import Foundation

extension UserDefaults {
    static var shared: UserDefaults {
        let appGroupId = "group.com.ant.test"
        return UserDefaults(suiteName: appGroupId)!
    }
}
