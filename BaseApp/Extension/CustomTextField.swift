//
//  TextField.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/01.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation
import UIKit


class CustomTextField: UITextField {
    /// 텍스트 크기
    @IBInspectable
    var textFont: String {
        get {
            switch font?.pointSize {
            case 10:
                return "smallest"
            case 12:
                return "smaller"
            case 14:
                return "small"
            case 16:
                return "normal"
            case 18:
                return "big"
            case 20:
                return "bigger"
            case 22:
                return "biggest"
            default:
                return "biggest"
            }
        }
        set {
            switch newValue {
            case "smallest":
                font = self.font?.withSize(FontSize.smallest.rawValue)
            case "smaller":
                font = self.font?.withSize(FontSize.smaller.rawValue)
            case "small":
                font = self.font?.withSize(FontSize.small.rawValue)
            case"normal":
                font = self.font?.withSize(FontSize.normal.rawValue)
            case "big":
                font = self.font?.withSize(FontSize.big.rawValue)
            case "bigger":
                font = self.font?.withSize(FontSize.bigger.rawValue)
            case "biggest":
                font = self.font?.withSize(FontSize.biggest.rawValue)
            default:
                font = self.font?.withSize(FontSize.biggest.rawValue)
            }
        }
    }
    
    /// 텍스트 힌트
    @IBInspectable
    var localizablePlaceHolder: String? {
        set { placeholder = NSLocalizedString(newValue!, comment: "") }
        get { return placeholder }
    }
    
    /// 텍스트 패딩
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y, width: bounds.size.width - paddingLeft - paddingRight, height: bounds.size.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}
